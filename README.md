# Mutant Challenge

Simple API REST to code challange at Mutant. The challange consist on build an application to fetch data from [Json Place Holder - Users](https://jsonplaceholder.typicode.com/users)

## Requirements

- [ ] Show all websites from all users
- [ ] Show Name, Email and company on alphabetical order
- [ ] Show all users that contains `suite` on address

## Bonus Points

- [ ] Save logs on elasticSearch
- [ ] Unit Tests

## How To Run the Project

### Cloud

You can access the application [here](https://mutant-test.herokuapp.com/api/users) using routes bellow

### Locally

#### Prerequisite

What you need and how to intall them:

- [Node.js](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/pt-BR/)
- [GIT](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

#### Clone Repository

On the directory of your choice run:

```bash
  git clone https://arthurgrigoletto@bitbucket.org/arthurgrigoletto/mutant-test.git
```

#### Install Dependencies

```bash
  cd mutant-test
  yarn install
```

#### Run

```bash
  docker-compose up -d
```

Then you can access `localhost:3333` with the routes bellow on your browser or some client applicantion that you prefer like `Insominia` or `Postman`

## Routes

| Endpoint                              | Description                            |
| ------------------------------------- | -------------------------------------- |
| GET `/api/users` **PUBLIC**           | Get All users                          |
| GET `/api/search_websites` **PUBLIC** | Get All Websites                       |
| GET `/api/search_basicInfo`**PUBLIC** | Get basicinfos asc sort                |
| GET `/api/search_address` **PUBLIC**  | Get all users with `suite` on address. |

## Built With

- [Node.js](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/pt-BR/)
- [Express](https://expressjs.com/pt-br/)
- [ESLint](https://eslint.org/)
- [Mogan](https://github.com/expressjs/morgan)
- [Cors](https://github.com/expressjs/cors)
- [Axios](https://github.com/axios/axios)
- [Sucrase](https://github.com/alangpierce/sucrase)
- [Typescript](https://www.typescriptlang.org/)
- [Prettier](https://prettier.io/)
- [Nodemon](https://nodemon.io/)
